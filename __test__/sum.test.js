import { expect, test } from "vitest"
import { sum } from "@/lib/sum.js"

test("adds 1 + 2 to equal 3", () => {
    expect(sum(1, 2)).toBe(3)
})

test("adds 1 + 2 and checks if the result is incorrect", () => {
    expect(sum(1, 2)).not.toBe(4)
})

test("checks if the inputs are numbers", () => {
    expect(typeof sum(1, 2)).toBe("number")
    expect(typeof sum("1", 2)).not.toBe("number")
    expect(typeof sum(1, "2")).not.toBe("number")
    expect(typeof sum("1", "2")).not.toBe("number")
})
