import { expect, test } from "vitest"
import { soustract } from "@/lib/soustract.ts"

test("adds 3 - 2 to equal 1", () => {
    expect(soustract(3, 2)).toBe(1)
})

test("adds 2 - 1 and checks if the result is incorrect", () => {
    expect(soustract(2, 1)).not.toBe(4)
})

test("checks if the inputs are numbers", () => {
    expect(typeof soustract(1, 2)).toBe("number")
})
