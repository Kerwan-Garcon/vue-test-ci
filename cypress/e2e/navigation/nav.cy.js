/// <reference types="cypress" />

context("Navigation", () => {
    beforeEach(() => {
        cy.visit("https://localhost:5173")
        // need to cy.get() the menu
    })

    it("cy.go() - go back or forward in the browser's history", () => {
        // clicking back
        cy.go(-1)
        cy.location("pathname").should("not.include", "a definir")

        // clicking forward
        cy.go(1)
        cy.location("pathname").should("include", "a definir")
    })
})
