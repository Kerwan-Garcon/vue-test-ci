export const users = [
    {
        id: 1,
        name: "Olivia Martin",
        avatarFallback: "OM",
        email: "",
        carts: [
            {
                name: "Carte grphique blablabla",
                quantity: 1,
            },
            {
                name: "Carte grphique blablabla",
                quantity: 0,
            },
        ],
    },
    {
        id: 2,
        name: "Jackson Lee",
        avatarFallback: "JL",
        email: "",
        carts: [
            {
                name: "Carte grphique blablabla",
                quantity: 0,
            },
            {
                name: "Carte grphique blablabla",
                quantity: 0,
            },
        ],
    },
    {
        id: 3,
        name: "Isabella Nguyen",
        avatarFallback: "IN",
        email: "",
        carts: [
            {
                name: "Carte grphique blablabla",
                quantity: 0,
            },
            {
                name: "Carte grphique blablabla",
                quantity: 0,
            },
        ],
    },
    {
        id: 4,
        name: "William Kim",
        avatarFallback: "WK",
        email: "",
        carts: [
            {
                name: "Carte grphique blablabla",
                quantity: 0,
            },
            {
                name: "Carte grphique blablabla",
                quantity: 0,
            },
        ],
    },
    {
        id: 5,
        name: "Sofia Davis",
        avatarFallback: "SD",
        email: "",
        carts: [
            {
                name: "Carte grphique blablabla",
                quantity: 0,
            },
            {
                name: "Carte grphique blablabla",
                quantity: 0,
            },
        ],
    },
]
