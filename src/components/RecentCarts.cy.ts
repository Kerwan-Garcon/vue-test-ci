import RecentCarts from "./RecentCarts.vue"
import { users } from "../utils/dumps/fake-users-datas"

describe("<RecentCarts />", () => {
    it("renders", () => {
        // see: https://on.cypress.io/mounting-vue
        cy.viewport(750, 750) // Set viewport to 750px x 750px
        cy.mount(RecentCarts)
    })

    it("input quantity should start at the right one", () => {
        cy.viewport(750, 750) // Set viewport to 750px x 750px
        cy.mount(RecentCarts)
        cy.get(".user-cart-0").click()
        cy.get("[data-cy=cart_counter_0]").should("have.value", users[0].carts[0].quantity)
    })

    it("should increment quantity", () => {
        cy.viewport(750, 750) // Set viewport to 750px x 750px
        cy.mount(RecentCarts)
        cy.get(".user-cart-0").click()
        cy.get("[data-cy=cart_counter_0]").should("have.value", users[0].carts[0].quantity)
        cy.get("[data-cy=plus-0]").click()
        cy.get("[data-cy=cart_counter_0]").should("have.value", users[0].carts[0].quantity + 1)
    })

    it("should decrement quantity", () => {
        cy.viewport(750, 750) // Set viewport to 750px x 750px
        cy.mount(RecentCarts)
        cy.get(".user-cart-0").click()
        cy.get("[data-cy=cart_counter_0]").should("have.value", users[0].carts[0].quantity)
        cy.get("[data-cy=minus-0]").click()
        cy.get("[data-cy=cart_counter_0]").should("have.value", users[0].carts[0].quantity - 1)
    })
})
