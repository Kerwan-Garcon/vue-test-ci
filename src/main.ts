import { createApp } from "vue"
import "./index.css"
import App from "./App.vue"
import ArticleShow from "./pages/articles/ArticleShow.vue"
import ArticleList from "./pages/articles/ArticleList.vue"
import Cart from "./pages/cart/Cart.vue"
import AdminDashboard from "./pages/AdminDashboard.vue"
import { createRouter, createWebHashHistory } from "vue-router"

const routes = [
    { path: "/", component: App },
    { path: "/articles", name: "article-list", component: ArticleList },
    { path: "/articles/:id", name: "article-show", component: ArticleShow },
    { path: "/cart", name: "cart", component: Cart },
    { path: "/dashboard", component: AdminDashboard },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes,
})

const app = createApp(App)

app.use(router)
app.mount("#app")
